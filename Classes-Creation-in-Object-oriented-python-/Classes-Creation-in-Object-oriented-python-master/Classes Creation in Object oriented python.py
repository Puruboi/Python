# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 16:19:14 2020

@author: Anonymous
"""
"""Class Creation in Object Oriented Python
Now we will work on Python and Classes. We will accomplish it by completing each task in the project:

Create the Class definition based on requirements.
Create the Constructor to allow us to create objects with data.
Add Getters and Setters to access and modify the attributes.
Add a String representation of the object for output.
Sort a list of objects."""
# prepare  a class 
class Car:
    make = "mahindra"
    model = "KUV 100 (K8+)"
    color = "red"
    year = "2018"
    price = 750000

# object    
car = Car()
#getting the attributes 
print("Model = " + car.model) # getting the attributes 

#Create Constructor 

class Cars:
    def __init__(self,make = "unkown",model = "unkown",
                 color = "unkown", year = 0,
                 price = 0):
        self.make = make
        self.model = model
        self.color = color
        self.year = year
        self.price = price

# Creating object    
#getting the attributes  
car_1 = Cars('maruti', 'swift', 'black', 2019,800000) # passing attributes 
print("Model = " + car_1.model) # getting the attributes

# getter and setter 

class Cars_1:
    def __init__(self,make = "unkown",model = "unkown",
                 color = "unkown", year = 0,
                 price = 0):
        self.make = make
        self.model = model
        self.color = color
        self.year = year
        self._price = price # indicates that the attributes to be hidden
    @property # property decorater 
    def price(self):
        return self._price
    @price.setter
    def price(self,p):
        if (p<=0):
            raise ValueError("Price is zero or least")
        print("Setter for price called")
        self._price = p
    # Create string Representation
    def __str__(self):
        return 'Car(make = '+ self.make + ', model = '+ str(self.model) +\
            ',color = ' +str(self.color) + ' year= '+str(self.year) +\
                ' price = '+str(self.price)+')'
                
    
# Creating object    
#getting the attributes  
car_2 = Cars_1('maruti', 'swift', 'black', 2019,800000) # passing attributes 
print("Model = " + car_1.model) # getting the attributes
#car_2.price = -1
car_2.price =800000
# print the object 
print('car:',car_2)

# sort objects read from file 
path = r"C:\Users\Anonymous\Desktop\Puruboi\cars.csv"
fh = open(path, "r")
cars_data = fh.readlines()
cars_data.pop(0)
cars_list = []

for rawstring in cars_data:
    make,model,color,year,price = rawstring.split(',')
    cars_list.append(Car(make,model,color,int(year),float(price)))

cars_list.sort(key=lambda car: car.price)

